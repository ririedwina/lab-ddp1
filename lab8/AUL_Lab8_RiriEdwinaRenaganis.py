class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        # TODO: Implementasikan getter untuk tipe!
        return self.__tipe

    def get_harga(self):
        # TODO: Implementasikan getter untuk harga!
        return self.__harga
    
    def get_tulisan(self):
        # TODO: Implementasikan getter untuk tulisan!
        return self.__tulisan
    
    def get_angka_lilin(self):
        # TODO: Implementasikan getter untuk angka_lilin!
        return self.__angka_lilin
    
    def get_topping(self):
        # TODO: Implementasikan getter untuk topping!
        return self.__topping

class KueSponge(KueUlangTahun):
    def __init__(self, tipe, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__(tipe, tulisan, angka_lilin, topping, harga)
        self.__rasa = rasa
        self.__warna_frosting = warna_frosting

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_rasa(self):
        return self.__rasa
    
    def get_warna_frosting(self):
        return self.__warna_frosting
    
class KueKeju(KueUlangTahun):
    def __init__(self, tipe, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__(tipe, tulisan, angka_lilin, topping, harga)
        self.__jenis_kue_keju = jenis_kue_keju

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_jenis_kue_keju(self):
        return self.__jenis_kue_keju

class KueBuah(KueUlangTahun):
    def __init__(self, tipe, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__(tipe, tulisan, angka_lilin, topping, harga)
        self.__jenis_kue_buah = jenis_kue_buah
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_jenis_kue_buah(self):
        return self.__jenis_kue_buah

# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    # TODO: Implementasikan menu untuk membuat custom bundle!
    print("Jenis kue: ")
    print("1. Kue Sponge \n2. Kue Keju \n3. Kue Buah")
    kwe = input("Pilih jenis kue: ")
    if kwe == '1':
        rasa = input("Pilih rasa (coklat/stroberi): ")
        frost = input("Pilih warna frosting (coklat/pink): ")
        top = input("Pilih topping (ceri/stroberi): ")
        umur = input("Masukkan angka lilin: ")
        ucap = input("Masukkan tulisan pada kue: ")
        pesenan =  KueSponge("Kue Sponge", ucap, umur, top, rasa, frost, 2500)
    
    if kwe == '2':
        jenku = input("Pilih jenis kue kejunya (New York/Japanese): ")
        top = input("Pilih topping (ceri/stroberi): ")
        umur = input("Masukkan angka lilin: ")
        ucap = input("Masukkan tulisan pada kue: ")
        pesenan =  KueKeju("Kue Keju", ucap, umur, top, jenku, 3000)

    if kwe == '3':
        rasa = input("Pilih rasa (coklat/stroberi): ")
        jenku = input("Pilih jenis kue buahnya (American/British): ")
        top = input("Pilih topping (ceri/stroberi): ")
        umur = input("Masukkan angka lilin: ")
        ucap = input("Masukkan tulisan pada kue: ")
        pesenan =  KueBuah("Kue Buah", ucap, umur, top, rasa, jenku, 3500)

    return pesenan

# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    # TODO: Implementasikan menu untuk memilih premade bundle!
    print("Pilihan paket istimewa:")
    print("1. New York-style Cheesecake with Strawberry Topping \n2. Chocolate Sponge Cake with Cherry Topping and Blue Icing \n3. American Fruitcake with Apple-Grape-Melon-mix Topping")
    
    paket = input("Pilih paket: ")
    umur = input("Masukkan angka lilin: ")
    ucap = input("Masukkan tulisan pada kue: ")
    
    if paket == '1':
        pesenan = KueUlangTahun("Kue Keju New York", "2700", ucap, umur, "Strawberry")
    if paket == '2':
        pesenan = KueUlangTahun("Kue Sponge Cokelat", "2200", ucap, umur, "Cherry Toping dan Icing Biru")
    if paket == '3':
        pesenan = KueUlangTahun("Kue Buah Amerika", "2200", ucap, umur, "Apel-Anggur-Melon")
    
    return pesenan


# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    # TODO: Implementasikan kode untuk print detail dari suatu kue!
    if kue.get_tipe() == "Kue Sponge":
        print("{} {} dengan topping {}".format(kue.get_tipe(),  kue.get_rasa(), kue.get_topping()))
    elif kue.get_tipe() == "Kue Keju":
        print("{} {} dengan topping {}".format(kue.get_tipe(),  kue.get_jenis_kue_keju(), kue.get_topping()))
    elif kue.get_tipe() == "Kue Buah":
        print("{} {} dengan topping {}".format(kue.get_tipe(),  kue.jenis_kue_buah(), kue.get_topping()))
    else:
        print(kue.get_tipe() + " dengan topping " + kue.get_topping())

    print("Tulisan ucapan yang anda inginkan adalah \"{}\"".format(kue.get_tulisan()))
    print("Angka lilin yang anda pilih adalah {}".format(kue.get_angka_lilin()))
    print("Harga: ¥{}".format(kue.get_harga()))

# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()