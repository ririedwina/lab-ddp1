import datetime;

class Karyawan():
    def __init__(self, username, password, nama, umur, role):
        self.__username = username
        self.__password = password
        self.__nama = nama
        self.__umur = umur
        self.__role = role

    def get_username(self):
        return self.__username

    def get_password(self):
        return self.__password

    def get_nama(self):
        return self.__nama
    
    def get_umur(self):
        return self.__umur
    
    def get_role(self):
        return self.__role

    def login(self):
        is_login = True
        ct = datetime.datetime.now()
        print("Selamat datang {}".format(self.__nama))
        return is_login, ct
    
    def logout(self):
        is_login = False
        return is_login

class Kasir(Karyawan):
    def __init__(self, username, password, nama, umur, role, diterima):
        super().__init__(username, password, nama, umur, role)
        self.__diterima = diterima

    def get_diterima(self, uang):
        return self.__diterima

    def kegiatan(self):
        print("Apa yang ingin anda lakukan? (Tulis angka saja) \n3. Terima pembayaran \n10. Logout")
    
    def bayar(self, uang_sekarang, uang_dibayar):
        if uang_dibayar.isnumeric():
            uang_sekarang += int(uang_dibayar)
            self.__diterima += int(uang_dibayar)
            print("Berhasil menerima uang sebanyak {}".format(uang_dibayar))
        else:
            print("Input tidak valid!")

        return uang_sekarang

class Janitor(Karyawan):
    def __init__(self, username, password, nama, umur, role, membersihkan = 0, berbelanja = 0):
        super().__init__(username, password, nama, umur, role)
        self.__membersihkan = membersihkan
        self.__berbelanja = berbelanja
    
    def get_membersihkan(self, angka):
        self.__membersihkan += angka
        return self.__membersihkan
    
    def get_berbelanja(self, angka):
        self.__berbelanja += angka
        return self.__berbelanja

    def kegiatan(self):
        print("4. Bersihkan toko \n5. Beli deterjen \n10. Logout")
    
    def belanja(self, deterjen_sekarang, deterjen):
        if deterjen.isdigit():
            deterjen_sekarang += int(deterjen)
            self.get_berbelanja(int(deterjen))
            print("Berhasil membeli {} deterjen".format(deterjen))
        else:
            print("Input tidak valid!")

        return deterjen_sekarang
    
    def bebersih(self, deterjen_sekarang):
        if deterjen_sekarang > 0:
            deterjen_sekarang -= 1
            global last_cleaned 
            last_cleaned= datetime.datetime.now()
            self.get_berbelanja(1)
            print("Toko berhasil dibersihkan")
        else:
            print("Persediaan deterjen tidak ada!")

        return last_cleaned, deterjen_sekarang

class Chef(Karyawan):
    def __init__(self, username, password, nama, umur, role, membuat, membuang):
        super().__init__(username, password, nama, umur, role)
        self.__membuat = membuat
        self.__membuang = membuang
    
    def get_membuat(self, kue):
        self.__membuat += kue
        return self.__membuat
    
    def get_membuang(self,kue):
        self.__membuang += kue
        return self.__membuang

    def kegiatan(self):
        print("Apa yang ingin anda lakukan? (Tulis angka saja) \n6. Buat kue \n7. Buang kue \n10. Logout")

    def buat(self, kue_sekarang, kue):
        if kue.isnumeric():
            kue_sekarang += int(kue)
            self.get_membuat(int(kue))
            print("Berhasil membuat {} kue".format(kue))
        else:
            print("Input tidak valid!")

        return kue_sekarang
    
    def buang(self, kue_sekarang, kue_dibuang):
        if kue_dibuang.isnumeric():
            if kue_sekarang >= int(kue_dibuang):
                kue_sekarang -= int(kue_dibuang)
                self.get_membuang(int(kue_dibuang))
                print("Berhasil membuang {} kue".format(kue_dibuang))
            else:
                print("Persediaan kue tidak ada!")
        else:
            print("Input tidak valid!")

        return kue_sekarang

data = {}
jk = 0
ju = 0
jc = 0
jd = 0
global last_cleaned
last_cleaned = None
is_login = False
looping = True
while looping:
    print("Selamat datang di Sistem Manajemen Homura")

    while is_login == False:
        print("Apa yang ingin anda lakukan? (Tulis angka saja) \n1. Register karyawan baru \n2. Login \n8. Status Report \n9. Karyawan Report \n11. Exit")
        pilih = input("Pilihan: ")
        if pilih == "1":
            print("Format data: [username] [password] [nama] [umur] [role]")
            register = input("Input data karyawan baru: ").split()
            if register[4].lower() == 'kasir':
                new_user = Kasir(register[0], register[1], register[2], register[3], register[4], 0)
            elif register[4].lower() == 'janitor':
                new_user = Janitor(register[0], register[1], register[2], register[3], register[4], 0, 0)
            elif register[4].lower() == 'chef':
                new_user = Chef(register[0], register[1], register[2], register[3], register[4], 0, 0)
                
            if register[0] not in data:
                data[register[0]] = new_user
                jk += 1
            print("Karyawan {} berhasil ditambahkan".format(register[2]))

        if pilih == "2":
            user = input("Username: ")
            pw = input("Password: ")
            if user.lower() in data:
                if data[user].get_password() == pw:
                    print("Selamat datang {}".format(user))
                    is_login = True
                else:
                    print("username/password salah. Silahkan coba lagi")
            else:
                print("username/password salah. Silahkan coba lagi")

        if pilih == "9":
            print("fitur masih dalam proses :D")
        
        if pilih == "8":
            print("=================================== \nSTATUS TOKO HOMURA SAAT INI \nJumlah Karyawan: {} \nJumlah Cash: {} \nJumlah Kue: {} \nJumlah deterjen: {} \nTerakhir kali dibersihkan: {} \n===================================".format(jk, ju, jc, jd, last_cleaned))
        
        if pilih == "11":
            print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
            looping = False
            break

        else:
            print("invalid input!")
            
    
    while is_login == True:
        if data[user].get_role() == 'chef':
            data[user].kegiatan()
            pilihan = input("Pilihan: ")
            if pilihan == '6':
                buat = input("Buat berapa kue? ")
                jc = data[user].buat(jc, buat)
            elif pilihan == '7':
                buang = input("Buang berapa kue? ")
                jc = data[user].buang(jc, buang)
            elif pilihan == '10':
                is_login = False
        elif data[user].get_role() == 'janitor':
            data[user].kegiatan()
            pilihan = input("Pilihan: ")
            if pilihan == '4':
                result = data[user].bebersih(jd)
                jd = result[1]
                last_cleaned = result[0] 
            elif pilihan == '5':
                tambah = input("Jumlah pembelian deterjen: ")
                jd = data[user].belanja(jd, tambah)
            elif pilihan == '10':
                is_login = False
        elif data[user].get_role() == 'kasir':
            data[user].kegiatan()
            pilihan = input("Pilihan: ")
            if pilihan == '3':
                tambah = input("Jumlah pembayaran: ")
                ju = data[user].bayar(ju, tambah)
            elif pilihan == '10':
                is_login = False
 