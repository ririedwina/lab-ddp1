def cekNama(pemenang, urutan, juara, hadiah):
    if pemenang.lower() in daftarPeserta:
        daftarPeserta[pemenang.lower()]['juara'].add(juara)
        daftarPeserta[pemenang.lower()]['hadiah'].append(daftarLomba[lomba_terurut[urutan]][hadiah])
        return
    else:
        print("Peserta tidak terdaftar, posisi juara dilewati")
        return
        
print("==================================")
print("  WELCOME TO KARTI-NIAN CHALLENGE ")
print("==================================")
print("")

jumlah_lomba = input("Masukkan jumlah acara yang akan dilombakan:")
while True:
    if jumlah_lomba.isdigit():
        break
    else:
        print("input salah!")
        jumlah_lomba = input("Masukkan kembali jumlah acara yang akan dilombakan:")
print("")

temp = 1
daftarLomba = {}
for i in range(int(jumlah_lomba)):
  hadiah = []
  print("LOMBA " + str(temp))
  temp += 1
  namaLomba = input("Nama lomba: ")
  juara1 = input("Hadiah juara 1: ")
  juara2 = input("Hadiah juara 2: ")
  juara3 = input("Hadiah juara 3: ")
  while True:
    if juara1.isdigit() and juara2.isdigit() and juara3.isdigit():
        break
    else:
        print("Nominal ada yang salah! Silahkan input ulang hadiahnya")
        juara1 = input("Hadiah juara 1: ")
        juara2 = input("Hadiah juara 2: ")
        juara3 = input("Hadiah juara 3: ")
        
  print("")
  hadiah.append(int(juara1))
  hadiah.append(int(juara2))
  hadiah.append(int(juara3))
  daftarLomba[namaLomba] = hadiah

print("==================================")
print("    DATA MATA LOMBA DAN HADIAH  ")
print("==================================")

lomba_terurut = sorted(daftarLomba)
urut = 0
for i in range(len(lomba_terurut)):
    print("")
    print("Lomba " + lomba_terurut[urut]) 
    print("[Juara 1] " + str(daftarLomba[lomba_terurut[urut]][0]))
    print("[Juara 2] " + str(daftarLomba[lomba_terurut[urut]][1]))
    print("[Juara 3] " + str(daftarLomba[lomba_terurut[urut]][2]))
    urut += 1

print("")
print("==================================")
print("     DATA PESERTA CHALLENGE")
print("==================================")
print("")
  
jumlah_peserta = int(input("Masukkan jumlah peserta yang berpartisipasi: "))
print("")

temp = 1
daftarPeserta = {}
for i in range(jumlah_peserta):
  peserta = input("Nama peserta " + str(temp) + ": ")
  daftarPeserta[peserta.lower()] = {'nama': peserta, 'juara': set(), 'hadiah': []}
  temp += 1

print("")
print("==================================")
print("       CHALLENGE DIMULAI")
print("==================================")

urutan = 0
for i in range(len(lomba_terurut)):
    print("")
    print("Lomba " + lomba_terurut[urutan]) 
    juara_satu = input("Juara 1: ")
    juara_dua = input("Juara 2: ")
    juara_tiga = input("Juara 3: ")

    if juara_satu == juara_dua or juara_dua == juara_tiga or juara_satu == juara_tiga:
        print("Terdapat kesamaan pemenang, hasil tidak sah. Hasil tidak dianggap.")
    else:
        cekNama(juara_satu, urutan, 1, 0) 
        cekNama(juara_dua, urutan, 2, 1)
        cekNama(juara_tiga, urutan, 3, 2)
    urutan += 1

print(daftarPeserta)
print("")
print("==================================")
print("          FINAL RESULT")
print("==================================")

hasilLomba = {}
hasilTerurut = sorted(daftarPeserta)
for i in hasilTerurut:
    daftarJuara = daftarPeserta[i]['juara']
    peringkat = sorted(daftarJuara)
    daftarHadiah = sum(daftarPeserta[i]['hadiah'])
    print(daftarPeserta[i]['nama'])
    print("Total hadiah: " + str(daftarHadiah))
    print("Juara yang pernah diraih: " + str(peringkat)[1:-1])
    print("")