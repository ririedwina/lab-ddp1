file1 = input("Masukkan nama file input: ")
file2 = input("Masukkan nama file output: ")

try:
    count = len(open(file1).readlines(  ))
    output_file = open(file2, "w")
    read = open(file1).read()
    new_str = ''
    temp =  0
    simpan = 0
    total = 0

    for input in read:    
        if input.isalpha():
            new_str = new_str + input
        elif input.isdigit():
            temp += int(input)
        elif input == " ":
            new_str = new_str + input
        
        total += 1
        if (input == "\n") or (total == len(read)):
            if temp % 2 == 0:
                simpan += 1
                print(new_str, file=output_file)
            new_str = ""
            temp = 0
        
    print(" ")    
    print("Total baris dalam file input: " + str(count))
    print("Total baris yang disimpan: " + str(simpan))

except FileNotFoundError:
    print("Nama file yang anda masukkan tidak ditemukan")