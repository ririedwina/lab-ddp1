def createMatrix(rowCount, colCount, dataList):
  # https://stackoverflow.com/questions/40120892/creating-a-matrix-in-python-without-numpy with modification
    mat = []
    index = 0
    for i in range(rowCount):
        rowList = []
        for j in range(colCount):
            rowList.append(int(dataList[index]))
            index += 1
        mat.append(rowList)
    return mat

def addMatrix(matA, matB):
  # https://www.programiz.com/python-programming/examples/add-matrix
    result = [[matA[i][j] + matB[i][j]  for j in range(len(matA[0]))] for i in range(len(matA))]
    # print(result)
    return result

def subMatrix(matA, matB):
    result = [[matA[i][j] - matB[i][j]  for j in range(len(matA[0]))] for i in range(len(matA))]
    return result

def determinan(matA):
    result = matA[0][0]*matA[1][1] - matA[0][1]*matA[1][0]
    return abs(result)
      
def transpose(matA):
  # https://www.programiz.com/python-programming/examples/transpose-matrix
    result = [[matA[j][i] for j in range(len(matA))] for i in range(len(matA[0]))]
    return result

def getInputMat(ukMatrix):
    row = int(ukMatrix[0])
    column = int(ukMatrix[2])
    listElemen = []
    for i in range(row):
        elemen = input(("Baris " + str(i+1) + " matrix: ")).split(" ")
        if(len(elemen)) != column:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = createMatrix(row, column, listElemen)
    return mat1

def getInputMat1(ukMatrix):
    row = int(ukMatrix[0])
    column = int(ukMatrix[2])
    listElemen = []
    for i in range(row):
        elemen = input(("Baris " + str(i+1) + " matrix 1: ")).split(" ")
        if(len(elemen)) != column:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = createMatrix(row, column, listElemen)
    return mat1

def getInputMat2(ukMatrix):
    row = int(ukMatrix[0])
    column = int(ukMatrix[2])
    listElemen = []
    for i in range(row):
        elemen = input(("Baris " + str(i+1) + " matrix 2: ")).split(" ")
        if(len(elemen)) != column:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = createMatrix(row, column, listElemen)
    return mat1

def mat2x2():
    row = 2
    column = 2
    listElemen = []
    for i in range(row):
        elemen = input(("Baris " + str(i+1) + " matrix: ")).split(" ")
        if(len(elemen)) != column:
            print("Terjadi kesalahan input. Silakan ulang kembali.")
            print()
            break
        else:
            listElemen += elemen
    mat1 = createMatrix(row, column, listElemen)
    return mat1

def printMat(matrix):
    print()
    print("Hasil dari operasi:")
    # https://java2blog.com/print-a-matrix-in-python/
    for row in matrix:
        for val in row:
            print(val, end=" ")
        print()
    print()

while(True):
    print("Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang dapat dilakukan:")
    print("1. Penjumlahan")
    print("2. Pengurangan")
    print("3. Transpose")
    print("4. Determinan")
    print("5. Keluar")
    op = input("Silakan pilih operasi: ")
    print()
    

    if op.isdigit():
        if int(op) == 1:
            ukMatrix = input("Ukuran matriks: ").split(" ")
            print()
            try:
                matrix1 = getInputMat1(ukMatrix)
                print()
                matrix2 = getInputMat2(ukMatrix)
                hasil = addMatrix(matrix1, matrix2)
                printMat(hasil)
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(op) == 2:
            ukMatrix = input("Ukuran matriks: ").split(" ")
            print()
            try:
                matrix1 = getInputMat1(ukMatrix)
                print()
                matrix2 = getInputMat2(ukMatrix)
                hasil = subMatrix(matrix1, matrix2)
                printMat(hasil)
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(op) == 3:
            ukMatrix = input("Ukuran matriks: ").split(" ")
            print()
            try:
                matrix = getInputMat(ukMatrix)
                hasil = transpose(matrix)
                printMat(hasil)
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(op) == 4:
            try:
                matrix = mat2x2()
                print()
                print("Hasil dari operasi: ")
                print(determinan(matrix))
            except:
                print("Terjadi kesalahan input. Silakan ulang kembali.")
                print()
                continue
        elif int(op) == 5:
            print("Sampai Jumpa!")
            break
    else:
        print("Terjadi kesalahan input. Silakan ulang kembali.")
        print()
