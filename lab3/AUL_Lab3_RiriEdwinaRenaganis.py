from random import randrange
print("Masukkan pesan yang akan dikirim:")
pesan_asli = input()
n = int(input("Pilih nilai n: "))
kunci = 20220310 - n
huruf = ""

for i in pesan_asli:
    if i.isupper() == True:
        n_huruf = n % 26
        if(ord(i) + n_huruf > 90):
            angka = (ord(i) + n_huruf) % 90 + 64
        else:
            angka = (ord(i) + n_huruf)
        huruf += chr(angka)
    elif i.islower() == True:
        n_huruf = n % 26
        if(ord(i) + n_huruf > 122):
            angka = (ord(i) + n_huruf) % 122 + 96
        else:
            angka = (ord(i) + n_huruf)
        huruf += chr(angka)
    elif i == " ":
        huruf += " "
    elif i.isdigit():
        n_angka = n % 5
        angka = int(i) + 2 * n_angka
        baru = str(angka)
        huruf += baru[-1]

paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. "

print(huruf)
temp = 0  
oreri = 0
while True:
    if oreri >= len(paragraf) or temp >= len(huruf):
        break
    # else:
    #     print(oreri, len(paragraf), temp, len(huruf), paragraf[oreri], huruf[temp])
    
    if huruf[temp] == " ":
        paragraf =  paragraf[:oreri] + "$" + paragraf[oreri:]
        temp = temp + 1

    elif paragraf[oreri].lower() != huruf[temp].lower():
        if huruf[temp].isdigit():
            paragraf =  paragraf[:oreri] + str(huruf[temp]) + paragraf[oreri:]
        # if huruf[temp] == " " or huruf[temp].isdigit():
            temp = temp + 1
    elif paragraf[oreri].lower() == huruf[temp].lower():  
        paragraf = paragraf[:oreri] + paragraf[oreri].upper() + paragraf[oreri+1:]
        temp = temp + 1
    oreri = oreri + 1

key_location = randrange(1, len(paragraf))
paragraf = paragraf[:key_location] + f"*{kunci}*" + paragraf[key_location:]
print(paragraf)