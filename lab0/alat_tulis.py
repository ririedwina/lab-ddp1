harga_pen = 3000
harga_pencil = 2000
harga_cortape = 5000

print("Daftar harga barang:")
print("Pulpen: " + str(harga_pen) + " /pc")
print("Pensil: " + str(harga_pencil) + " /pc")
print("Correction tape: " + str(harga_cortape) + " /pc")

print("Masukan jumlah pesanan anda: ")
jum_pen = int(input("Jumlah pen: "))
jum_pencil = int(input("Jumlah pencil: "))
jum_cortape = int(input("Jumlah correction tape: "))

total_pembelian = (harga_pen*jum_pen) + (harga_pencil*jum_pencil) + (harga_cortape*jum_cortape)

print("Ringkasan Pembelian")
print(f"{jum_pen} pulpen x {harga_pen}")
print(f"{jum_pencil} pensil x {harga_pencil}")
print(f"{jum_cortape} correction tape x {harga_cortape}")
print("Total pembayaran: " + str(total_pembelian))