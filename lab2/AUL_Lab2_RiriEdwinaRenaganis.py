hpe = 100
hpr = 100
round = 1
mnh = 1

while not(hpe < 0 or hpr < 0):
    print("Hp Ei: " + str(hpe))
    print("Hp Raiden: " + str(hpr))
    print("Putaran ke " + str(round))
    print("==========================")
    if(round%3 == 0):
        print("Raiden bersiap menggunakan \"The Final Calamity\"")
    print("Apa yang ingin anda lakukan?")
    print("1. Menyerang")
    print("2. Menghindar")
    if mnh == 1:
        print("3. Musou no Hitotachi")
    jurus = int(input("Masukkan pilihan anda: "))
    
    if(round % 3):
        if(jurus == 1):
            hpr -= 20
            hpe -= 20
            round += 1
            print("==========================")
            print("Ei menyerang Raiden dan mengurangi Hp Raiden sebanyak 20")
            print("Raiden menyerang Ei dan mengurangi Hp Ei sebanyak 20")
            print("==========================")
        elif(jurus == 2):
            round += 1
            print("==========================")
            print("Raiden menyerang namun Ei berhasil menghindar")
            print("==========================")
        elif(jurus == 3):
            round += 1
            hpe -= 20
            if(mnh == 1):
                hpr -= 50
                mnh -= 1
                print("==========================")
                print("Ei menyerang \"Musou no Hitotachi\" dan mengurangi Hp Raiden sebanyak 50")
                print("Raiden menyerang Ei dan mengurangi Hp Ei sebanyak 20")
                print("==========================")
    else:
        if(jurus == 1):
            hpr -= 20
            hpe -= 50
            round += 1
            print("==========================")
            print("Ei menyerang Raiden dan mengurangi Hp Raiden sebanyak 20")
            print("Raiden menyerang Ei dan mengurangi Hp Ei sebanyak 50")
            print("==========================")
        elif(jurus == 2):
            round += 1
            print("==========================")
            print("Raiden menyerang namun Ei berhasil menghindar")
            print("==========================")
        elif(jurus == 3):
            round += 1
            hpe -= 50
            if(mnh == 1):
                hpr -= 50
                mnh -= 1
                print("==========================")
                print("Ei menyerang \"Musou no Hitotachi\" dan mengurangi Hp Raiden sebanyak 50")
                print("Raiden menyerang Ei dan mengurangi Hp Ei sebanyak 50")
                print("==========================")

if(hpe > hpr):
    print("Ei memenangkan pertarungan")
elif(hpr > hpe):
    print("Reiden memenangkan pertarungan")
else:
    print("Pertarungan seri")